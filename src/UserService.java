
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Nutcha1223
 */
public class UserService {

    private static ArrayList<User> list = new ArrayList<>();

    public static ArrayList<User> getList() {
        return list;
    }
    

    public static void loadUser() {
        File f = null;
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            f = new File("testuser.bin");
            fis = new FileInputStream(f);
            ois = new ObjectInputStream(fis);
            list = (ArrayList<User>) ois.readObject();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TestRead.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TestRead.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(TestRead.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        System.out.println("loadUser");
    }

    public static void saveUser(ArrayList<User> list) {
        UserService.list = list;
        File f = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {
            f = new File("testuser.bin");
            fos = new FileOutputStream(f);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(list);
        } catch (FileNotFoundException ex) {
        } catch (IOException ex) {
        } finally {
            try {
                if (fos != null) {
                    fos.close();
                }
                if (oos != null) {
                    oos.close();
                }
            } catch (IOException ex) {
            }
        }
        
        System.out.println("saveUser");
    }

    public static boolean auth(String loginName, String password) {
       for(User u:list){
            if(u.getUser().equals(loginName)&&u.getPassword().equals(password)){
                return true;
            }
        }
        return false;
    }

}
