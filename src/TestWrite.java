
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Nutcha1223
 */
public class TestWrite {

    public static void main(String[] args) {
        File f = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        User user1 = new User("user1", "password");
        User user2 = new User("user2", "pass");
        User user3 = new User("war", "war");
        User user4 = new User("yin", "yin");
        ArrayList<User> list = new ArrayList();
        list.add(user1);
        list.add(user2);
        list.add(user3);
        list.add(user4);
        for (User a : list) {
            System.out.println(a);
        }
        try {
            f = new File("testuser.bin");
            fos = new FileOutputStream(f);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(list);
        } catch (FileNotFoundException ex) {
        } catch (IOException ex) {
        } finally {
            try {
                if (fos != null) {
                    fos.close();
                }
                if (oos != null) {
                    oos.close();
                }
            } catch (IOException ex) {
            }
        }

    }
}
